
class Task:
    def __init__(self, title, description, assignee=None):
        self.title = title
        self.description = description
        self.assignee = assignee

    def __str__(self):
        return f'{self.title} -> {self.assignee}'

