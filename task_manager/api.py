import requests

from task_manager.task import Task
from task_manager.task_manager import TaskManager


class RemoteTaskManager(TaskManager):
    REMOTE_ADDRESS = 'https://jsonplaceholder.typicode.com/todos'

    def fetch_data(self):
        response = requests.get(self.REMOTE_ADDRESS)
        if response.status_code == 200:
            return response.json()

    def load_tasks(self):
        tasks_raw = self.fetch_data()
        if tasks_raw:
            for task in tasks_raw:
                self.add_task(Task(task.get('title', ''),
                                   task.get('description', '')))
        else:
            raise Exception("Unable to load tasks")

