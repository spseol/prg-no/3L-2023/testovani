
import pytest

from task_manager.task import Task
from task_manager.task_manager import TaskManager
from task_manager.user import User


def test_task_assignment_integration():
    task_manager = TaskManager()

    user1 = User("Alice")
    user2 = User("Bob")
    task1 = Task("Integration Task 1", "Task for integration testing")
    task2 = Task("Integration Task 2", "Another task for integration testing")

    task_manager.add_user(user1)
    task_manager.add_user(user2)
    task_manager.add_task(task1)
    task_manager.add_task(task2)

    task_manager.assign_task(task1, user1)
    task_manager.assign_task(task2, user2)

    assert task1.assignee == user1
    assert task2.assignee == user2
