from task_manager.task import Task
from task_manager.task_manager import TaskManager
from task_manager.user import User


def test_task_assignee_after_user_creation():
    task_manager = TaskManager()

    user = User("Charlie")
    task = Task("Regression Task", "Testing regression")

    task_manager.add_user(user)
    task_manager.add_task(task)

    # Assign task to user
    task_manager.assign_task(task, user)

    # Regression test: Check if the task's assignee is still the same after creating a new user
    new_user = User("David")
    task_manager.add_user(new_user)

    assert task.assignee == user
