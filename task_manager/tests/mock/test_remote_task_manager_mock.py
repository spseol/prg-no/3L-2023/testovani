from unittest.mock import patch, MagicMock

from task_manager.api import RemoteTaskManager


# TODO ukázat v debugu
def test_fetch_data():
    fake_data = [{'id': 1, 'title': "Task 1"}, {'id': 2, 'title': "Task 2"}]

    # Použití patch decoratoru pro mockování volání requests.get
    with patch('requests.get') as mock_get:
        # Vytvoření falešné odpovědi od API
        mock_response = MagicMock()
        mock_response.json.return_value = fake_data
        mock_response.status_code = 200
        mock_get.return_value = mock_response

        # Volání testované metody
        api_client = RemoteTaskManager()
        result = api_client.fetch_data()
        assert result == fake_data
