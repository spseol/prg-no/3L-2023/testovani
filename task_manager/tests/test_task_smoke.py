from task_manager.task import Task
from task_manager.task_manager import TaskManager
from task_manager.user import User


def test_task_manager_smoke():
    task_manager = TaskManager()

    task = Task("Smoke Task", "Testing smoke test")
    user = User("Eve")

    task_manager.add_task(task)
    task_manager.add_user(user)

    task_manager.assign_task(task, user)

    assert task.assignee == user
