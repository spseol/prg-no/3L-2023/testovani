from telnetlib import EC

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

from selenium.webdriver.support.wait import WebDriverWait

# Inicializace WebDriveru (v tomto případě pro Google Chrome)
driver = webdriver.Chrome()

try:

    # Otevření Google
    driver.get("https://www.sport.cz/")
    wait = WebDriverWait(driver, 100)
    # Najděte pole pro vyhledávání pomocí názvu elementu


    # Počkejte chvíli na získání výsledků
    time.sleep(3)

    # Vypište tituly prvních pěti výsledků
    for i in range(5):
        result = driver.find_element("tag name", "h4")
        print(result.text)

finally:
    # Zavřete prohlížeč
    driver.quit()