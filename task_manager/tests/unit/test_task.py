from task_manager.task import Task


def test_task_creation():
    task = Task("Write unit tests", "Write unit tests for Task class.")
    assert task.title == "Write unit tests"
    assert task.description == "Write unit tests for Task class."
    assert task.assignee is None