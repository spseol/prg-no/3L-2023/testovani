from task_manager.task import Task
from task_manager.task_manager import TaskManager
from task_manager.user import User


def test_task_manager_add_task():
    task_manager = TaskManager()
    task = Task("Test Task", "Testing task manager functionality")
    task_manager.add_task(task)
    assert len(task_manager.tasks) == 1
    assert task_manager.tasks[0] == task


def test_task_manager_add_user():
    task_manager = TaskManager()
    user = User("Alice")
    task_manager.add_user(user)
    assert len(task_manager.users) == 1
    assert task_manager.users[0] == user

