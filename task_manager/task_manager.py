
class TaskManager:
    def __init__(self):
        self.tasks = []
        self.users = []

    def add_task(self, task):
        self.tasks.append(task)

    def add_user(self, user):
        self.users.append(user)

    def assign_task(self, task, user):
        task.assignee = user
