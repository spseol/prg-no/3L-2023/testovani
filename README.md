# Task manager

## Install dependencies:
```bash
pip install -r requirments.txt

```

## Run tests:
```bash
python -m pytest
```

## Playwright
```bash
playwright install
```