class Calculator:
    def add(self, a, b):
        return a + b

    def subtract(self, a, b):
        return a - b

    def multiply(self, a, b):
        return a * b

    def divide(self, a, b):
        if b == 0:
            raise ValueError("Cannot divide by zero.")
        return a / b

    def factorial(self, n):
        if n < 0:
            raise ValueError("Factorial is not defined for negative numbers.")
        if n == 0 or n == 1:
            return 1
        return n * self.factorial(n - 1)

    def add_complex(self, complex1: tuple, complex2: tuple):
        real_part = complex1[0] + complex2[0]
        imag_part = complex1[1] + complex2[1]
        return real_part, imag_part

    def subtract_complex(self, complex1, complex2):
        real_part = complex1[0] - complex2[0]
        imag_part = complex1[1] - complex2[1]
        return (real_part, imag_part)


