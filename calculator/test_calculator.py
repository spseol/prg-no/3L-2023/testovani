from calculator import Calculator
import pytest


@pytest.fixture
def calc():
    return Calculator()


# Testovací funkce pro sčítání
def test_addition(calc):
    result = calc.add(3, 4)
    assert result == 7


# Testovací funkce pro odčítání
def test_subtraction(calc):
    result = calc.subtract(8, 5)
    assert result == 3


# Testovací funkce pro násobení
def test_multiplication(calc):
    result = calc.multiply(2, 6)
    assert result == 12


# Testovací funkce pro dělení
def test_division(calc):
    result = calc.divide(10, 2)
    assert result == 5


# Testovací funkce pro faktoriál
def test_factorial_positive(calc):
    result = calc.factorial(5)
    assert result == 120


def test_factorial_zero(calc):
    result = calc.factorial(0)
    assert result == 1


def test_factorial_negative_raises_error(calc):
    with pytest.raises(ValueError, match="Factorial is not defined for negative numbers."):
        calc.factorial(-3)


# Testovací funkce pro sčítání komplexních čísel
def test_add_complex(calc):
    result = calc.add_complex((1, 2), (3, 4))
    assert result == (4, 6)


# Testovací funkce pro odčítání komplexních čísel
def test_subtract_complex(calc):
    result = calc.subtract_complex((5, 7), (2, 3))
    assert result == (3, 4)